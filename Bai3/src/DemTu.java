import java.util.Scanner;

public class DemTu {
	private String GetData() {
		Scanner sc = new Scanner(System.in);
		String buffer;
		System.out.print("Nhap cau can dem tu (an enter 2 lan de thoat): ");
		StringBuilder stb = new StringBuilder();
		do {
			buffer = sc.nextLine();
			if(buffer.length()!=0) {
				stb.append(buffer);
				stb.append('\n');
			}
		}while(buffer.length()!=0);
		sc.close();
		return new String(stb);
	}
	public DemTu() {
		String sentence = GetData();
		int len=0;
		int count = 0;
		for(int i=0;i<sentence.length();i++) {
			if(sentence.charAt(i)==' ' || sentence.charAt(i)=='\n' || sentence.charAt(i) =='\t') {
				if(len>0) {
					count++;
				}
				len = 0;
			}else {
				len++;
			}
		}
		System.out.println("So tu: "+count);
	}
	public static void main(String[] args) {
		new DemTu();
	}
}
