import java.util.ArrayList;
import java.util.Scanner;

public class SoNguyenTo {
	private ArrayList<Integer> listSNT;
	private boolean checkSNT(int n) {
		for (Integer i : listSNT) {
			if(i*i>n) break;
			if(n%i==0) return false;
		}
		if(listSNT.indexOf(n)<0) listSNT.add(n);
		return true;
	}
	public SoNguyenTo() {
		listSNT = new ArrayList<Integer>();
		listSNT.add(2);
		Scanner sc = new Scanner(System.in);
		int n;
		System.out.print("Nhap so can tinh: ");
		n = sc.nextInt();
		try {
			if(n<=1) throw new Exception("So can nhap phai la so lon hon 1");
			for(int i=3;i<n;i++) {
				checkSNT(i);
			}
			System.out.println("Cac so nguyen to nho hon "+n+" :");
			for (Integer i : listSNT) {
				System.out.print(i+ " ");
			}
		}catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			sc.close();
		}
	}
	public static void main(String[] args) {
		new SoNguyenTo();
	}
}
