import java.util.ArrayList;
import java.util.Scanner;

public class SapXepVaChen {
	private ArrayList<Integer> list;
	public SapXepVaChen() {
		System.out.println("Nhap mang: ");
		list = NhapDuLieu();
		SapXep();
		System.out.println("Mang sau khi sap xep: ");
		InDuLieu();
		System.out.println("\nNhap gia tri can chen: ");
		Scanner sc = new Scanner(System.in);
		int x = sc.nextInt();
		ChenDL(x);
		System.out.println("Sau khi chen du lieu");
		InDuLieu();
		sc.close();
	}
	private void InDuLieu() {
		for(int item:list) {
			System.out.print(item+" ");
		}
	}
	private void ChenDL(int x) {
		int size = list.size();
		int l=0, r=size-1;
		int mid=(l+r)/2;
		while(l<=r) {
			if(list.get(mid)==x) {
				list.add(mid,x);
				return;
			}
			if(x<list.get(mid)) {
				r = mid-1;
			}
			if(x>list.get(mid)) {
				l = mid+1;
			}
			mid = (l+r)/2;
		}
		if(mid==0) {
			list.add(0,x);
		}else if(x<list.get(mid)) {
			list.add(mid-1,x);
		}else {
			list.add(mid+1,x);
		}
	}
	private void SapXep() {
		int size = list.size();
		for(int i=0;i<size-1;i++) {
			for(int j=i;j<size;j++) {
				if(list.get(i)>list.get(j)) {
					int temp = list.get(i);
					list.set(i, list.get(j));
					list.set(j, temp);
				}
			}
		}
	}
	private ArrayList<Integer> NhapDuLieu(){
		ArrayList<Integer> list = new ArrayList<Integer>();
		Scanner sc =  new Scanner(System.in);
		try {
			String buffer = sc.nextLine();
			String[] token = buffer.split(" ");
			for(String str:token) {
				list.add(Integer.parseInt(str));
			}
			return list;
		}finally {
			sc.close();
		}
	}
	public static void main(String[] args) {
		new SapXepVaChen();
	}
}
