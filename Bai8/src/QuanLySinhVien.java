import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

class QuanLySinhVien {
	private ArrayList<SinhVien> listSV;
	private Scanner sc;
	public void DisplayMenu() {
		System.out.println("Select task: ");
		System.out.println("1. Add student");
		System.out.println("2. Edit student by ID");
		System.out.println("3. Delete student by ID");
		System.out.println("4. Sort student by gpa");
		System.out.println("5. Sort student by name");
		System.out.println("6. Show student");
		System.out.println("0. Exit");
	}
	private int NhapSoNguyen() {
		int num=0;
		while(!sc.hasNextInt()) {
			sc.nextLine();
		}
		num = sc.nextInt();
		sc.nextLine();
		return num;
	}
	private float NhapSoThuc() {
		float num=0;
		while(!sc.hasNextFloat()) {
			sc.nextLine();
		}
		num = sc.nextFloat();
		sc.nextLine();
		return num;
	}
	public int SelectTask() {
		int task=99, desc;
		String url;
		System.out.println("Nhap cong viec can tim: ");
		if(sc.hasNextInt()) {
			task = sc.nextInt();
		}
		sc.nextLine();
		switch (task) {
		case 1:
			System.out.println("Nhap file: ");
			url = sc.nextLine();
			NhapSinhVien(url);
			break;
		case 2:
			SuaSinhVien();
			break;
		case 3:
			XoaSinhVien();
			break;
		case 4:
			System.out.println("0 tang dan, 1 = giam dan: ");
			desc = sc.nextInt();
			sc.nextLine();
			SapXepTheoGPA((desc==0)? false:true);
			break;
		case 5:
			System.out.println("0 tang dan, 1 = giam dan: ");
			desc = sc.nextInt();
			sc.nextLine();
			SapXepTheoTen((desc==0)? false:true);
			break;
		case 6:
			InDuLieu();
			break;

		case 0:
			return -1;
		}
		return 0;
	}
	private void InDuLieu() {
		for (SinhVien sv : listSV) {
			System.out.println("ID: "+sv.getId());
			System.out.println("\tTen: "+sv.getName());
			System.out.println("\tTuoi: "+sv.getAge());
			System.out.println("\tDia chi: "+sv.getAddress());
			System.out.println("\tDiem trung binh: "+sv.getGpa());
		}
	}
	private void SapXepTheoTen(boolean desc) {
		int size = listSV.size();
		for(int i=0;i<size-1;i++) {
			for(int j=i;j<size;j++) {
				if(desc) {
					if(listSV.get(i).getName().compareTo(listSV.get(j).getName())<0) {
						Swap(i,j);
					}
				}else {
					if(listSV.get(i).getName().compareTo(listSV.get(j).getName())>0) {
						Swap(i,j);
					}
				}
			}
		}
	}
	private void SapXepTheoGPA(boolean desc) {
		int size = listSV.size();
		for(int i=0;i<size-1;i++) {
			for(int j=i;j<size;j++) {
				if(desc) {
					if(listSV.get(i).getGpa()<listSV.get(j).getGpa()) {
						Swap(i,j);
					}
				}else {
					if(listSV.get(i).getGpa()>listSV.get(j).getGpa()) {
						Swap(i,j);
					}
				}
			}
		}
	}
	private void Swap(int i, int j) {
		SinhVien temp = listSV.get(i);
		listSV.set(i,listSV.get(j));
		listSV.set(j, temp);
	}
	private void XoaSinhVien() {
		System.out.println("Nhap id can tim: ");
		String id = sc.nextLine();
		int size = listSV.size();
		int i;
		for( i= 0;i<size;i++) {
			if(listSV.get(i).getId().equals(id)) {
				break;
			}
		}
		if(i>=size) {
			System.out.println("Khong tim duoc sinh vien");
			return;
		}else {
			listSV.remove(i);
		}
	}
	private void SuaSinhVien() {
		System.out.println("Nhap id can tim: ");
		String id = sc.nextLine();
		int size = listSV.size();
		int i;
		for( i= 0;i<size;i++) {
			if(listSV.get(i).getId().equals(id)) {
				break;
			}
		}
		if(i>=size) {
			System.out.println("Khong tim duoc sinh vien");
			return;
		}else {
			SinhVien sv = listSV.get(i);
			System.out.println("Nhap ten sinh vien (khong nhap = khong thay doi):");
			String name = sc.nextLine();
			if(name.length()!=0) {
				sv.setName(name);
			}
			
			System.out.println("Nhap tuoi sinh vien (0 = khong thay doi):");
			int age = NhapSoNguyen();
			if(age!=0) {
				sv.setAge(age);
			}
			
			System.out.println("Nhap dia chi sinh vien (khong nhap = khong thay doi):");
			String address = sc.nextLine();
			if(address.length()!=0) {
				sv.setAddress(address);
			}
			
			System.out.println("Nhap diem trung binh cua sinh vien (0 = khong thay doi):");
			float gpa = NhapSoThuc();
			if(gpa!=0) {
				sv.setGpa(gpa);
			}
			
		}
	}
	private void NhapSinhVien(String url) {
		if(url.equals("")) {
			System.out.println("Nhap thong tin:");
			while(true) {
				System.out.println("\tNhap id:");
				SinhVien sv = new SinhVien();
				String id = sc.nextLine();
				if(id.length()==0) break;
				sv.setId(id);
				System.out.println("\tNhap ten:");
				sv.setName(sc.nextLine());
				System.out.println("\tNhap tuoi:");
				sv.setAge(NhapSoNguyen());
				System.out.println("\tNhap dia chi: ");
				sv.setAddress(sc.nextLine());
				System.out.println("\tNhap diem trung binh: ");
				sv.setGpa(NhapSoThuc());
				listSV.add(sv);
				System.out.println("Them thanh cong");
			}
		}else {
			File file = new File(url);
			FileReader fread=null;
			BufferedReader bread = null;
			try {
				fread = new FileReader(file);
				bread  = new BufferedReader(fread);
				String buffer;
				int task=0;
				buffer = bread.readLine();
				SinhVien sv=null;
				while(buffer!=null) {
					switch(task) {
					case 0:
						sv = new SinhVien();
						sv.setId(buffer);
						break;
					case 1:
						sv.setName(buffer);
						break;
					case 2:
						sv.setAge(Integer.parseInt(buffer));
						break;
					case 3:
						sv.setAddress(buffer);
						break;
					case 4:
						sv.setGpa(Float.parseFloat(buffer));
						listSV.add(sv);
						break;
					}
					buffer = bread.readLine();
					task++;
					task%=5;
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}finally {
				if(fread!=null) {
					try {
						fread.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				if(bread!=null) {
					try {
						bread.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}
	public QuanLySinhVien() {
		sc = new Scanner(System.in);
		try {
			listSV = new ArrayList<SinhVien>();
			while(true) {
				DisplayMenu();
				if(SelectTask()==-1) break;
			}
		}finally {
			sc.close();
		}
	}
	public static void main(String[] args) {
		new QuanLySinhVien();
	}
}
