import java.util.ArrayList;
import java.util.Scanner;

public class SapXep {
	public int[] NhapDuLieu() {
		Scanner sc = new Scanner(System.in);
		String buffer = sc.nextLine();
		int[] arr;
		try {
			String[] token = buffer.split(" ");
			ArrayList<Integer> lst = new ArrayList<Integer>();
			for(int i = 0;i<token.length;i++) {
				if(token[i].length()!=0) {
					lst.add(Integer.parseInt(token[i]));
				}
			}
			arr = new int[lst.size()];
			int index=0;
			for(int num:lst) {
				arr[index++]=num;
			}
		}finally {
			sc.close();
		}
		return arr;
	}
	public SapXep() {
		System.out.println("Nhap mang can sap xep: ");
		int[] arr = NhapDuLieu();
		for(int i=0;i<arr.length-1;i++) {
			for(int j=i;j<arr.length;j++) {
				if(arr[i]<arr[j]) {
					int temp = arr[i];
					arr[i] = arr[j];
					arr[j] = temp;
				}
			}
		}
		System.out.println("Mang sau khi duoc sap xep: ");
		for(int num:arr) {
			System.out.print(num+" ");
		}
	}
	public static void main(String[] args) {
		new SapXep();
	}

}
