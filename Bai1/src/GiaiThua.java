import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class GiaiThua {
	public GiaiThua(){
		String buffer;
		int n,res = 1;
		BufferedReader bread = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Nhap so can tinh: ");
		try {
			buffer = bread.readLine();
			n = Integer.parseInt(buffer);
			if(n<0) throw new Exception("So can nhap phai la so khong am");
			for (int i = 2; i <= n; i++) {
				res*=i;
			}
			System.out.println(n+"! = "+res);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			try {
				bread.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	public static void main(String[] args) {
			new GiaiThua();
	}

}
