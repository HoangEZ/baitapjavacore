import java.util.ArrayList;
import java.util.Scanner;

public class InMang {
	private ArrayList<Integer>list;
	private ArrayList<Integer>count;
	private int[] NhapMang() {
		ArrayList<Integer> buffer = new ArrayList<Integer>();
		Scanner sc = new Scanner(System.in);
		try {
			while(true) {
				String line = sc.nextLine();
				if(line.equals("")) {
					break;
				}
				String[] token = line.split(" ");
				for(String element:token) {
					if(!element.equals("")) {
						buffer.add(Integer.parseInt(element));
					}
				}
			}
			int[] arr = new int[buffer.size()];
			int index = 0;
			for (int i : buffer) {
				arr[index++] = i;
			}
			return arr;
		}finally {
			sc.close();
		}
	}
	public InMang() {
		System.out.println("Nhap chuoi so can tinh (Enter x2 = Ket thuc): ");
		int[] arr = NhapMang();
		list = new ArrayList<Integer>();
		count =  new ArrayList<Integer>();
		for(int i:arr) {
			int index = list.indexOf(i);
			if(index<0) {
				list.add(i);
				count.add(1);
			}else {
				int num = count.get(index)+1;
				count.set(index, num);
			}
		}
		
		//in du lieu
		System.out.println("Cac phan tu chi xuat hien 1 lan: ");
		for(int i=0;i<list.size();i++) {
			if(count.get(i)==1) {
				System.out.println(list.get(i));
			}
		}
	}
	public static void main(String[] args) {
		new InMang();
	}

}
