import java.util.LinkedList;
import java.util.Scanner;

public class DemTu {
	private LinkedList<String> list;
	private LinkedList<Integer> count;
	private String NhapCau() {
		Scanner sc = new Scanner(System.in);
		String buff = sc.nextLine();
		sc.close();
		return buff;
	}
	private void InDuLieu() {
		for(int i=0;i<list.size();i++) {
			System.out.println(list.get(i)+" : "+count.get(i));
		}
	}
	public DemTu() {
		list = new LinkedList<String>();
		count =  new LinkedList<Integer>();
		System.out.println("Nhap cau can dem: ");
		String sentence = NhapCau();
		String[] token = sentence.split(" ");
		for(int i=0;i<token.length;i++) {
			int index = list.indexOf(token[i]);
			if(index<0) {
				list.add(token[i]);
				count.add(1);
			}else {
				int temp = count.get(index).intValue();
				temp++;
				count.set(index, temp);
			}
		}
		System.out.println("Ket qua: ");
		InDuLieu();
	}
	public static void main(String[] args) {
		new DemTu();
	}

}
